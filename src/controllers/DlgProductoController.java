/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import beans.Producto;
import daoimpl.IDaoImplProducto;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

/**
 *
 * @author Jadpa28
 */
public class DlgProductoController {
    private IDaoImplProducto daoEmpleado;

    public DlgProductoController() {
        daoEmpleado = new IDaoImplProducto();
    }
    
    public void save(Producto e) throws IOException{
        daoEmpleado.save(e);
    }
    
    public void update(Producto e) throws IOException{
        daoEmpleado.update(e);
    }
    
    public String saveImageProlife(File profilePicture) throws IOException {
        String profileDirPath = System.getProperty("user.dir")+"\\ProfilePicture";
        File profileDir = new File(profileDirPath);
        
        if(!profileDir.exists())
            profileDir.mkdir();
        
        Path src = profilePicture.toPath();
        Path dst = profileDir.toPath();
        Files.copy(src, dst.resolve(src.getFileName()), StandardCopyOption.REPLACE_EXISTING);
        
        return src.toAbsolutePath().toString();
        
    }
}
