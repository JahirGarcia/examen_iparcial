/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import beans.Producto;
import daoimpl.IDaoImplProducto;
import java.io.IOException;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Jadpa28
 */
public class IFrmGestionProductoController {
    private IDaoImplProducto daoProducto;
        private String header[] = {"Codigo","Nombre","Descripcion","Cantidad",
        "Precio"};

    public IFrmGestionProductoController() {
        daoProducto = new IDaoImplProducto();
    }
    
    public List<Producto> getAll() throws IOException {
        return this.daoProducto.findAll();
    }
    
    public void delete(Producto e) throws IOException{
        daoProducto.delete(e);
    }
    
    public DefaultTableModel getTableModel() throws IOException{
        List<Producto> empleados = daoProducto.findAll();
        Object[][] data = new Object[empleados.size()][header.length];
        int i = 0;
        for(Producto e : empleados){
            data[i++] = getData(e);
        }
        
        return new DefaultTableModel(data, header);
    }
    
    public Object[] getData(Producto e){
        Object[] singleData = new Object[header.length];
        singleData[0] = e.getCodProducto();
        singleData[1] = e.getNombre();
        singleData[2] = e.getDescripcion();
        singleData[3] = e.getCantidad();
        singleData[4] = e.getPrecio();
        
        return singleData;
    }
    
    public TableRowSorter getTableRowSorter() throws IOException{
        return new TableRowSorter(getTableModel());
    }
}
