/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author Yasser
 */
public class Producto {
    private int id;//4
    private String codProducto;//13
    private String nombre;//63
    private String descripcion;//203
    private int cantidad;//4
    private double precio;//8
    private String rutaimagen;//203

    public Producto() {
    }

    public Producto(String codProducto, String nombre, String descripcion, int cantidad, double precio, String rutaimagen) {
        this.codProducto = codProducto;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.precio = precio;
        this.rutaimagen = rutaimagen;
    }
    
    
    public Producto(int id, String codProducto, String nombre, String descripcion, int cantidad, double precio, String rutaimagen) {
        this.id = id;
        this.codProducto = codProducto;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.precio = precio;
        this.rutaimagen = rutaimagen;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodProducto() {
        return codProducto;
    }

    public void setCodProducto(String codProducto) {
        this.codProducto = codProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getRutaimagen() {
        return rutaimagen;
    }

    public void setRutaimagen(String rutaimagen) {
        this.rutaimagen = rutaimagen;
    }

    @Override
    public String toString() {
        return "Producto{" + "id=" + id + ", codProducto=" + codProducto + ", nombre=" + nombre + ", descripcion=" + descripcion + ", cantidad=" + cantidad + ", precio=" + precio + ", rutaimagen=" + rutaimagen + '}';
    }
    
    
}
