/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import beans.Producto;
import java.io.IOException;


/**
 *
 * @author Yasser
 */
public interface IDaoProducto extends IDao<Producto> {
    Producto buscarPorId(int id)throws IOException;
    Producto buscarPorCodProducto(String codProd)throws IOException;
}
